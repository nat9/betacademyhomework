﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{

    public class Node
    {


        private int Value;

        public int getValue()
        {
            return Value;
        }
        public void setValue(int v)
        {
            Value = v;
        }


        public Node Next;

        public Node GetNext()
        {
            return Next;
        }
        public void SetNext(Node n)
        {
            Next = n;
        }

        public Node(int v)
        {
            Value = v;
            Next = null;
        }

    }

    class List
    {


        int Lenght;
        Node Head;


        public List()
        {
            Lenght = 0;
            Head = null;
        }

        public List(int l, Node h)
        {
            l = Lenght;
            Head = h;
        }

        public Node GetHead()
        {
            return Head;
        }
        public int GetLenght()
        {
            return Lenght;
        }


        public Node First()
        {
            return Head;
        }

        //1.Adding a node at the end of the list
        public void Add(int v)
        {
            Node NewNode = new Node(v);
            Node current = Head;
            if (Head == null)
            {

                Head = NewNode;
            }
            else
            {
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = NewNode;
            }
            Lenght++;
        }

        //2.Adding a node at the beginning of the list
        public void AddBeginning(int v)
        {
            Node NewNode = new Node(v);
            if (Head == null)
            {
                Head = NewNode;
            }
            else
            {
                NewNode.Next = Head;
                Head = NewNode;
            }
        }
        //3.Adding a node after the given node
        public void AddAfter(Node n, int v)
        {
            Node NewNode = new Node(v);
            if (n.Next == null)
            {
                n.Next = NewNode;
            }
            else
            {
                NewNode.Next = n.Next;
                n.Next = NewNode;
            }
        }
        //4.Print nodes
        public void PrintNodes()
        {
            Node current = Head;
            if (current == null)
            {
                Console.WriteLine("No more nodes to display.");
                Console.WriteLine();
            }
            else
            {

                while (current != null)
                {
                    Console.WriteLine("Node : " + current.getValue());
                    current = current.Next;
                }
                Console.WriteLine();
            }


        }

        //5.Remove a node
        internal void Remove(int d)
        {


            Node current = Head;

            if (current != null)
            {

                if (current.getValue() == d)
                {

                    if (current.Next != null)
                    {
                        current = current.Next;
                    }
                    else
                    {
                        current = null;
                    }
                    Head = current;
                    Lenght--;
                }
                else
                {
                    while (current.Next != null && current.Next.getValue() != d)
                    {
                        current = current.Next;
                    }
                    if (current.Next != null && current.Next.getValue() == d)
                    {

                        current.Next = current.Next.Next;
                        current = null;
                        Lenght--;
                    }
                    else
                    {
                        Console.WriteLine(d.ToString() + " could not be found in the list.");
                    }
                }
            }

        }



        //6.Remove after
        public int RemoveAfter(int d)
        {


            int count = 0;

            Node current = Head;

            if (current!=null)
            {
               
               int l = 0;
               while (current.getValue() != d && current.Next!=null)
               {
                    l++;
                    current = current.Next;

                        
               }
               if (current.getValue() == d && current.Next != null)
               {
                    count = GetLenght() - l + 1;
                    current.Next = null;
                    current = null;
                }


            }
            else
            {
                Console.WriteLine(d.ToString() + " could not be found in the list.");
            }
            return count;
        }
            
            

        
    }
}
