using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {

            List list = new List();
            for(int i = 0; i < 9; i++)
            {
               
                list.Add(i);
            }
            list.PrintNodes();
            list.AddBeginning(300);
            list.PrintNodes();
            Node h = list.GetHead();
            list.AddAfter(h, 123456);
            list.PrintNodes();

            list.Remove(5);
            list.Remove(123456);
            list.PrintNodes();

            int k=list.RemoveAfter(2);
            list.PrintNodes();
            Console.WriteLine("The count of deleted elements:" + " " + k.ToString());

            Console.ReadKey();

        }
    }
}
