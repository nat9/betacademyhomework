﻿using System;
class Name
{
    public interface UniversityMember
    { 
    }

     public class Student :  UniversityMember
    {
        protected string name;
        protected string surename;
        protected int course;
        protected string faculty;


        public Student()
        {
            name = " ";
            surename = " ";
            course = 0;
            faculty = " ";
        }

        public Student(string n, string s, int c, string f)
        {
            

            if (n == " ")
            {
                Console.WriteLine("Enter name!");
            }
            else
            {
                name = n;
            }
            

            if (s == " ")
            {
                Console.WriteLine("Enter surename!");
            }
            else
            {
                surename = s;
            }
            
            if (c<1 || c> 4)
            {
                Console.WriteLine("Enter valid age!");
            }
            else
            {
                course = c;
            }

            faculty = f;
        }

        public string getName()
        {
            return name;
        }
        public void setName(string n)
        {
            name = n;
        }
        public string getSurename()
        {
            return surename;
        }
        public void setSurename(string s)
        {
            surename = s;
        }
        public int getCourse()
        {
            return course;
        }

        public void setCourse(int c)
        {
            course = c;
        }
        public string getFaculty()
        {
            return faculty;
        }
        public void setFaculty(string f)
        {
            faculty = f;
        }
    }

    public class Professor: UniversityMember
    {
        protected string name;
        protected string surename;
        protected string faculty;
        protected string ambion;

        public Professor()
        {
            name = " ";
            surename = " ";
            faculty =" ";
            ambion = " ";
        }
        public Professor(string n, string s, string f, string a)
        {

            if (n == " ")
            {
                Console.WriteLine("Enter name!");
            }
            else
            {
                name = n;
            }


            if (s == " ")
            {
                Console.WriteLine("Enter surename!");
            }
            else
            {
                surename = s;
            }

            if(f == " ")
            {
                Console.WriteLine("Enter the name of faculty!");
            }
            else
            {
                faculty = f;
            }
            if(a == " ")
            {
                Console.WriteLine("Enter the name of ambion!");
            }
            else
            {
                ambion = a;
            }
        }

        public string getName()
        {
            return name;
        }
        public void setName(string n)
        {
            name = n;
        }

        public string getSurename()
        {
            return surename;
        }
        public void setSurename(string s)
        {
            surename = s;
        }

        public string getFaculty()
        {
            return faculty;
        }
        public void setFaculty(string f)
        {
            faculty = f;
        }

        public string getAmbion()
        {
            return ambion;
        }
        public void setAmbion(string a)
        {
            ambion = a;
        }
    }


    static void Main()
    {
       Student a =new Student("Anna","Margaryan",2, "Informatics and Applied Mathematics");
    }

}